# Prova de Conceito: Rasa


## 1. Motivação

Testar um modelo de conversação utilizando o rasa baseado na reunião com a Ilha Soft

## 2. Modo de uso

### 2.1 Treinar 
```$ rasa train```

### 2.2 Testar no Terminal
```$ rasa shell```

###  2.3 Lançar o serviço no Telegram

Tenha um bot no telegram com o BotFather, vamos precisar dessas credenciais

Existem dois arquivos que você deve prestar atenção:
* credentials.yml
* endpoints.yml

#### 2.3.1 Configurando as credenciais

No credentials.yml, use esse template para o telegram

---
```
telegram:
  access_token: "seu_token"
  verify: "o nome do bot"
  webhook_url: "sua_https_url/webhooks/telegram/webhook" 
```
---

Para ter uma url de acesso externo, você pode usar o ngrok da seguinte forma:

``$ ngrok http 5005``

Veja a url https e substitua no "sua_https_url", ela muda a cada ativação do ngrok

#### 2.3.2 Configurando os endpoints (SUJEITO A EDIÇÃO)

No endpoints.yml, apenas coloque 

```
action_endpoint:
 url: "http://localhost:5055/webhook"
```

#### 2.3.3 Lançando o serviço

Após fazer os passos anteriores, siga essa sequencia:

1. Abra dois terminais no projeto
2. Em um deles use ``$ rasa run actions``
3. Tenha o ngrok executando com a url utilizada nas credenciais
4. No outro terminal, use ``$ rasa run``
5. pronto!