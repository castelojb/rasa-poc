## intent:cumprimentar
- Ei
- Olá
- Oi
- bom Dia
- boa noite


## intent:depedida
- tchau
- adeus
- até a próxima
- até logo

## intent:afirmativo
- sim
- de fato
- claro
- isso soa bem
- correto

## intent:negar
- não
- Nunca
- Acho que não
- não gosto disso
- de jeito nenhum
- Na verdade não

## intent:bom_humor
- perfeito
- muito bom
- ótimo
- surpreendente
- Maravilhoso
- Estou me sentindo muito bem
- Estou estou ótimo
- Eu estou bem

## intent:humor_infeliz
- triste
- muito triste
- infeliz
- ruim
- muito mal
- horrível
- Terrível
- não muito bom
- extremamente triste
- tão triste

## intent:desafio_de_bot
- Você é um robô?
- Você é um humano?
- estou falando com um bot?
- estou falando com um humano?