## happy path
* cumprimentar
  - utter_greet
* bom_humor
  - utter_happy

## sad path 1
* cumprimentar
  - utter_greet
* humor_infeliz
  - utter_cheer_up
  - utter_did_that_help
* afirmativo
  - utter_happy

## sad path 2
* cumprimentar
  - utter_greet
* humor_infeliz
  - utter_cheer_up
  - utter_did_that_help
* negar
  - utter_goodbye

## say goodbye
* depedida
  - utter_goodbye

## bot challenge
* desafio_de_bot
  - utter_iamabot
