#### This file contains tests to evaluate that your bot behaves as expected.
#### If you want to learn more, please see the docs: https://rasa.com/docs/rasa/user-guide/testing-your-assistant/

## happy path 1
* cumprimentar: Olá!
  - utter_greet
* bom_humor: maravilhoso
  - utter_happy

## happy path 2
* cumprimentar: Olá
  - utter_greet
* mood_great: maravilhoso
  - utter_happy
* despedida: bye-bye!
  - utter_goodbye

## sad path 1
* cumprimentar: ola
  - utter_greet
* humor_infeliz: não muito bem
  - utter_cheer_up
  - utter_did_that_help
* afirmativo: sim
  - utter_happy

## sad path 2
* cumprimentar: ola
  - utter_greet
* humor_infeliz: não bem
  - utter_cheer_up
  - utter_did_that_help
* negar: Na verdade não
  - utter_goodbye

## sad path 3
* cumprimentar: oi
  - utter_greet
* humor_infeliz: muito terrivel
  - utter_cheer_up
  - utter_did_that_help
* negar: não
  - utter_goodbye

## say goodbye
* despedida: bye-bye!
  - utter_goodbye

## bot challenge
* desafio_de_bot: Você é um robô?
  - utter_iamabot
