## intent:cumprimentar
- Oi
- Ola
- Oi bot
- Ola bot
- Olá!
- Bom dia
- oi de novo
- oi pessoal

## intent:despedida
- Tchau
- boa noite
- Adeus
- Até mais
- tchau tchau
- tenho que ir
- despedida

## intent:agradecimento
- Obrigado
- Obrigada
- Muito obrigado
- Obrigado bot
- obrigado por isso
- Felicidades

## intent: ask_cnh/conceito
- O que é CNH?
- O que quer dizer CNH?
- Para que preciso da CNH?
- Por que tirar a CNH?

## intent: ask_cnh/precos
- Esse serviço é gratuito?
- Quanto custa para obter a CNH?
- Quanto eu vou gastar para tirar a CNH?
- Esse serviço é pago?
- Tem algum desconto?

## intent: ask_cnh/requisitos
- Quais os requisitos para tirar a CNH?
- Existe algum requisito para obter a CNH?
- Eu posso tirar a CNH?

## intent: ask_cnh/documentos
- Quais os documentos necessários para tirar a CNH?
- Eu preciso levar alguma coisa para obter a CNH?
- Quais os papeis que eu preciso levar para tirar a CNH?

## intent: ask_cnh/etapas
- Quais são as etapas para ter a CNH?
- Qual o procedimento para ter a CNH?
- Como se tira a CNH?
- Como eu obtenho a CNH?

## intent: ask_cnh/tempo
- Qual o tempo médio para obter a CNH?
- Quanto tempo eu vou esperar para obter a CNH?
- Quando a CNH vai sair?

## intent: ask_cnh/atendimento
- Quais os horarios de atendimento?
- Quando eu posso fazer um atendimento?
- Quais os horarios de funcionamento?
- Quando posso ir fazer um atendimento?

## intent: ask_cnh/endereço
- Onde eu posso fazer o atendimento presencial?
- Onde fica o atendimento presencial?
- Onde eu tenho que ir para ser atendido?
- Qual o endereço do atendimento?

## intent: ask_cnh/acesso
- Quais as formas de acesso?
- Como posso acessar os serviços da CNH?
- Onde estão os serviços da CNH?