## greet + goodbye
* cumprimentar: Oi!
  - utter_greet
* despedida: Tchau
  - utter_bye

## greet + thanks
* cumprimentar: Olá
  - utter_greet
* agradecimento: muitíssimo obrigado
  - utter_noworries

## greet + thanks + goodbye
* cumprimentar: Oi!
  - utter_greet
* agradecimento: obrigado
  - utter_noworries
* despedida: Tchau
  - utter_bye