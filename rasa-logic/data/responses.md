## conceito
* ask_cnh/conceito
    - É a obtenção da Carteira Nacional de Habilitação(Carteira de Motorista), de forma definitiva, para condutores que possuem a permissão para dirigir.

## precos
* ask_cnh/precos
    - O serviço possui tarifas, ver [site](https://cartadeservicos.ce.gov.br/ConsultaCesec/pg_cs_servico_detalhe.aspx?idservico=539)

## requisitos
* ask_cnh/requisitos
    - A CNH definitiva é concedida a condutores, que, ao término de (um) ano, não tenham cometido nenhuma infração de natureza grave ou gravíssima, e não seja reincidente em infração média. Além do não cometimento das infrações citadas, o condutor não poderá ter acumulado pontos decorrentes de infrações leves nesse período.

## documentos
* ask_cnh/documentos
    - Carteira Nacional de Habilitação- CNH - Permissão ou Documento de Identidade (original e cópia); Comprovante de endereço - original e cópia, conforme Portaria nº / DETRAN-CE; Caso o condutor não possua comprovante de endereço no seu nome, poderá apresentar Declaração de Residência

## etapas    
* ask_cnh/etapas
    - Presencial. Solicitar senha ou aguardar chamada por ordem de chegada;. Apresentar a documentação exigida ao funcionário responsável pelo serviço;. Aguardar atendimento e o registro dos dados do solicitante no sistema pelo funcionário responsável;. Após o registro das informações no sistema, será gerada um boleto para o recolhecimento da taxa desse serviço, para efetuação do pagamento nas Agências Bancárias: Banco do Brasil, Caixa Econômica, Bradesco, Casas Lotéricas ou outros correspondentes bancários conveniados;. Após o pagamento da taxa, aguadar o envio da CNH definitiva pelos correios.Internet (on-line). Acessar o endereço eletrônico: https://sistemas.detran.ce.gov.br/central ;. Clicar na opção CNH Definitiva;. Informar no formulário eletrônico as seguintes informações: No item "Escolha a forma de acesso" seleciona uma das opções: Possuo a Habilitação em mãos; Perdi minha Habilitação ou ª Habilitação; CPF do condutor; Número da CNH ou PPD. Realizar a impressão ou download do boleto para o recolhecimento da taxa desse serviço, para efetuação do pagamento nas Agências Bancárias: Banco do Brasil, Caixa Econômica, Bradesco, Casas Lotéricas ou outros correspondentes bancários conveniados;. Após o pagamento, aguadar o envio da CNH definitiva pelos correios.Observações. Ainda permaneço no mesmo endereço que recebi a minha permissão de CNH:Não há necessidade de comparecimento ao DETRAN. Não moro mais no endereço que recebi a minha permissão de CNH:O condutor deverá comparecer ao DETRAN para atualizar o endereço e abrir o processo de CNH definitiva

## tempo
* ask_cnh/tempo
    - dias

## atendimento
* ask_cnh/atendimento
    - Sede do Detran e demais postos: h às hs; Postos em shopping: h às h

## endereco
* ask_cnh/endereço
    - Sede do Detran Maraponga e demais postos.

## acesso
* ask_cnh/acesso
    - Presencial Sede do Detran, no endereço: Avenida Godofredo Maciel, 2.900 - Maraponga, Fortaleza - CE, 60710-001 e Postos de Atendimento: Endereços: https://www.detran.ce.gov.br/postos-de-atendimento/ Online: https://sistemas.detran.ce.gov.br/central