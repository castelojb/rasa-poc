## atendimento (/tmp/tmph12nkfy8/c2ad174242f24eb5b57fca84767bdd43_conversation_tests.md)
* cumprimentar: ola
    - utter_greet
* ask_cnh: Quando eu posso ir ser atendido?
    - respond_ask_cnh
* agradecimento: Boa!   <!-- predicted: despedida: Boa! -->
    - utter_bye


## endereço (/tmp/tmph12nkfy8/c2ad174242f24eb5b57fca84767bdd43_conversation_tests.md)
* cumprimentar: ola
    - utter_greet
* ask_cnh: Onde eu posso ser atendido?
    - respond_ask_cnh
* agradecimento: Valeu   <!-- predicted: cumprimentar: Valeu -->
    - utter_bye   <!-- predicted: utter_greet -->


## conceito (/tmp/tmph12nkfy8/c2ad174242f24eb5b57fca84767bdd43_conversation_tests.md)
* cumprimentar: Bom dia!
    - utter_greet
* ask_cnh: O que é cnh?
    - respond_ask_cnh
* agradecimento: Valeu!   <!-- predicted: cumprimentar: Valeu! -->
    - utter_bye   <!-- predicted: utter_greet -->


## requisitos (/tmp/tmph12nkfy8/c2ad174242f24eb5b57fca84767bdd43_conversation_tests.md)
* cumprimentar: Fala bot!
    - utter_greet
* ask_cnh: O que eu devo ter para conseguir a cnh?
    - respond_ask_cnh
* agradecimento: Valeu!   <!-- predicted: cumprimentar: Valeu! -->
    - utter_bye   <!-- predicted: utter_greet -->


## etapas (/tmp/tmph12nkfy8/c2ad174242f24eb5b57fca84767bdd43_conversation_tests.md)
* cumprimentar: boa noite   <!-- predicted: despedida: boa noite -->
    - utter_greet   <!-- predicted: utter_bye -->
* ask_cnh: Qual o passo a passo para tirar a cnh?
    - respond_ask_cnh
* agradecimento: obrigado
    - utter_bye   <!-- predicted: utter_noworries -->


## tempo (/tmp/tmph12nkfy8/c2ad174242f24eb5b57fca84767bdd43_conversation_tests.md)
* cumprimentar: boa tarde   <!-- predicted: despedida: boa tarde -->
    - utter_greet   <!-- predicted: utter_bye -->
* ask_cnh: Quanto tempo demora para conseguir a cnh?
    - respond_ask_cnh
* agradecimento: obrigada
    - utter_bye   <!-- predicted: utter_noworries -->


