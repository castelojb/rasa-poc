## greet + goodbye
* cumprimentar: Oi!
  - utter_greet
* despedida: Tchau
  - utter_bye

## greet + thanks
* cumprimentar: Olá
  - utter_greet
* agradecimento: muitíssimo obrigado
  - utter_noworries

## greet + thanks + goodbye
* cumprimentar: Oi!
  - utter_greet
* agradecimento: obrigado
  - utter_noworries
* despedida: Tchau
  - utter_bye
  
## conceito
* cumprimentar: Bom dia!
  - utter_greet
* ask_cnh: O que é cnh?
  - respond_ask_cnh
* agradecimento: Valeu!
  - utter_bye

## precos
* ask_cnh: quanto custa uma cnh?
  - respond_ask_cnh
* despedida: Tchau
  - utter_bye

## requisitos
* cumprimentar: Fala bot!
  - utter_greet
* ask_cnh: O que eu devo ter para conseguir a cnh?
  - respond_ask_cnh
* agradecimento: Valeu!
  - utter_bye
  
## documentos
* cumprimentar: eai
  - utter_greet
* ask_cnh: Quais documentos eu preciso para a cnh?
  - respond_ask_cnh
* despedida: adeus
  - utter_bye

## etapas
* cumprimentar: boa noite
  - utter_greet
* ask_cnh: Qual o passo a passo para tirar a cnh?
  - respond_ask_cnh
* agradecimento: obrigado 
  - utter_bye

## tempo
* cumprimentar: boa tarde
  - utter_greet
* ask_cnh: Quanto tempo demora para conseguir a cnh?
  - respond_ask_cnh
* agradecimento: obrigada
  - utter_bye

## atendimento
* cumprimentar: ola
  - utter_greet
* ask_cnh: Quando eu posso ir ser atendido?
  - respond_ask_cnh
* agradecimento: Boa! 
  - utter_bye

## endereço
* cumprimentar: ola
  - utter_greet
* ask_cnh: Onde eu posso ser atendido?
  - respond_ask_cnh
* agradecimento: Valeu 
  - utter_bye

## acesso
* cumprimentar: ola
  - utter_greet
* ask_cnh: Onde eu posso acessar a cnh?
  - respond_ask_cnh
* despedida: Tchau  
  - utter_bye 
  